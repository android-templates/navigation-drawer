package com.turo.template_navigation_drawer;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    android.support.v7.widget.Toolbar appBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigationView = findViewById(R.id.main_nav_view);
        drawerLayout = findViewById(R.id.main_nav_drawer);
        appBar = findViewById(R.id.main_app_bar);

        // This is the action performed by the Menu Button on the Toolbar
        appBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Gravity.Starts means the Navigation Drawer should transition to the foreground
                drawerLayout.openDrawer(Gravity.START);
            }
        });

        /*
            This manages the actions performed by each option of the Nav. Drawer
            These options are declared @ res > menu > nav_items.xml
        */
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_items_1: Toast.makeText(MainActivity.this, "You pressed item 1", Toast.LENGTH_SHORT).show();
                    break;
                    case R.id.nav_items_2: Toast.makeText(MainActivity.this, "You pressed item 2", Toast.LENGTH_SHORT).show();
                    break;
                    case R.id.nav_items_3: Toast.makeText(MainActivity.this, "You pressed item 3", Toast.LENGTH_SHORT).show();
                    break;
                    case R.id.nav_items_4: Toast.makeText(MainActivity.this, "You pressed item 4", Toast.LENGTH_SHORT).show();
                    break;
                }
                drawerLayout.closeDrawer(Gravity.START);
                return true;
            }
        });
    }
}
